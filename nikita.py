#pollimine
import RPi.GPIO as GPIO #importimine viikudega töötamise jaoks
import time #pausid

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM) #Viikude poole pöördumine viis
                       #BCMi puhul nummerdamine käib pinnite numbrite kaudu (GPIO.XX)
GPIO.setup(2, GPIO.OUT) #setuppime, et pin(viik) 17 on autode punane
GPIO.setup(3, GPIO.OUT) #setuppime, et pin(viik) 27 on autode kollanet
GPIO.setup(4, GPIO.OUT) #setuppime, et pin(viik) 22 on autode roheline

GPIO.setup(21, GPIO.OUT) #setuppime, et pin(viik) 23 on inimeste punane
GPIO.setup(20, GPIO.OUT) #setuppime, et pin(viik) 24 on inimeste roheline

GPIO.setup(26, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #nuppu setuppimine
                                                     #pull up/pull down resistorite abil
                                                     #tema abil saame settida vaikeväärtust
GPIO.setup(16, GPIO.OUT) #settupime, et pin(viik) 25 on sinine tuli

#muutujad, et oleks mugavam
ar = 2
ay = 3
ag = 4

ir = 21
ig = 20

blue = 16
afvõiif = 0 #muuutuja, mille abiga me saame aru kas meil peab olema
            #AUTODEFOORI WHILE TSÜKKEL või INIMESTE FOORI TSÜKKEL
            #ja kui autodel on punane foor ja nupp oli vajutud, siis inimestel on
            #roheline foor ja parast jalle punane

def button(): #nuppu funktsioon
    global afvõiif #selleks, et afvõiif isegi fun. buttonis oli globaalne
    #kui toimub nuppu vajutus, siis nupp põleb
    #ja triggerime inimeste foori tsükkli
    if GPIO.input(26):
        #sinine nupp
        GPIO.output(blue, GPIO.HIGH)
        time.sleep(2.5)
        GPIO.output(blue, GPIO.LOW)
        afvõiif = 1 #selleks, et triggerida inimeste foori tsükkli

def main(): #main ehk siis autode foori töö ja inimeste foori töö
    global afvõiif #siin saamamodi teeme afvõiif globaalseks
    while True:
        while (afvõiif != 1): #kui ta on 0
            #kui nupp ei olnud vajutatud, siis muutuja afvõiif ei ole 1
            #kui afvõiif ei ole 1, siis alati meil kestab autode foori tsükkel
            
            #inimestel punane
            #autodel 5s punane, 1s kollane, 5s roheline, vilgub kollane, jälle punane
            GPIO.output(ir, GPIO.HIGH)
            button()
            GPIO.output(ar, GPIO.HIGH)
            button()
            time.sleep(5)
            button()
            GPIO.output(ar, GPIO.LOW)
            button()
            GPIO.output(ay, GPIO.HIGH)
            button()
            time.sleep(1)
            button()
            GPIO.output(ay, GPIO.LOW)
            button()
            GPIO.output(ag, GPIO.HIGH)
            button()
            time.sleep(5)
            button()
            GPIO.output(ag, GPIO.LOW)
            button()
            for i in range(3):
                GPIO.output(ay, GPIO.LOW)
                button()
                time.sleep(0.35)
                button()
                GPIO.output(ay, GPIO.HIGH)
                button()
                time.sleep(0.35)
                button()
            GPIO.output(ay, GPIO.LOW)
            button()
            #selle tsükkli lõpus on meil alati autodel on punane tuli
            #pärast kui autodel on punane tuli, kohe toimub afvoiif kontroll
            #kui nupp oli vajutud, siis algab inimeste foori tsükkel
            #ehk inimeste foori tsükkel alati algab sellega, et autode foor on PUNANE
            GPIO.output(ar, GPIO.HIGH)
            button()
        #kui aga muutuja afvõiif on 1, siis see tähendab seda, et oli juba event
        #ehk nuppu vajutamine toimus ja fun. button tegi oma tööd
        #alustame tsükkli inimeste fooriga
        #roheline foor inimestel
        time.sleep(1)
        button()
        GPIO.output(ir, GPIO.LOW)
        button()
        time.sleep(1)
        button()
        GPIO.output(ig, GPIO.HIGH)
        button()
        time.sleep(3)
        button()
        #roheline inimestel lõpetab, algab punane
        GPIO.output(ig, GPIO.LOW)
        button()
        #inimeste foori tsükkel lõpetab ja tema lõppus paneme, et afvõiif = 0
        #sellega, meil pärast jälle hakkab autode foori tsükkel
        afvõiif = 0
        #nüüd kui inimeste foori tsükkel juba kestas 5s 
        #ja sellel ajal autode foor oli punane (5s)
        #alustame pärast seda selle sama auto foori tsükkli sisega ilma whileta
        #ja ilma selleta, et autode foor põleb veel 5s
        GPIO.output(ir, GPIO.HIGH)
        button()
        GPIO.output(ar, GPIO.LOW)
        button()
        time.sleep(0.5)
        button()
        GPIO.output(ay, GPIO.HIGH)
        button()
        time.sleep(1)
        button()
        GPIO.output(ay, GPIO.LOW)
        button()
        GPIO.output(ag, GPIO.HIGH)
        button()
        time.sleep(5)
        button()
        GPIO.output(ag, GPIO.LOW)
        button()
        for i in range(3):
            GPIO.output(ay, GPIO.LOW)
            button()
            time.sleep(0.35)
            button()
            GPIO.output(ay, GPIO.HIGH)
            button()
            time.sleep(0.35)
            button()
        GPIO.output(ay, GPIO.LOW)
        button()
        #viimane tehe siin on autode kollane ehk siis pärast jälle algab tsükkel
        #sellega, et inimeste foor on punane ja autode foor on punane 5s

#põhiprogramm
#kõik LEDid ei põle
GPIO.output(ar, GPIO.LOW)
GPIO.output(ay, GPIO.LOW)
GPIO.output(ag, GPIO.LOW)
GPIO.output(ir, GPIO.LOW)
GPIO.output(ig, GPIO.LOW)
GPIO.output(blue, GPIO.LOW)
#kasutaja alustab programmi
message = input("Press 'Enter' to start")
time.sleep(1)

#algab autode foori tsükkel
main()

#selleks, et programmi täiesti lõpetada kuna kogu programm on suur while tsükkel
#tuleb RasPi terminalis jälle seda tööle panna (siis kõik LEDid ei põle
# enne enteri vajutamist)