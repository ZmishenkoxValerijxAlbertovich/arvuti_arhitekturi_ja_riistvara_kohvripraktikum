import RPi.GPIO as GPIO
import time

button_pressed = False
    
def yellow_car_ar():
    GPIO.output(auto_valgusfoor[0], GPIO.LOW)
    GPIO.output(auto_valgusfoor[1], GPIO.HIGH)
    
def green_car():
    GPIO.output(auto_valgusfoor[1], GPIO.LOW)
    GPIO.output(auto_valgusfoor[2], GPIO.HIGH)
    
def yellow_car_ag():
    GPIO.output(auto_valgusfoor[2], GPIO.LOW)
    GPIO.output(auto_valgusfoor[1], GPIO.HIGH)
    time.sleep(0.4)   
    GPIO.output(auto_valgusfoor[1], GPIO.LOW)
    time.sleep(0.4)
    GPIO.output(auto_valgusfoor[1], GPIO.HIGH)
    time.sleep(0.4)   
    GPIO.output(auto_valgusfoor[1], GPIO.LOW)
    time.sleep(0.4)
    GPIO.output(auto_valgusfoor[1], GPIO.HIGH)
    time.sleep(0.4)   
    GPIO.output(auto_valgusfoor[1], GPIO.LOW)

def callback(chanel):
    global button_pressed
    nupp_oli_vajatud = True
    GPIO.output(16, GPIO.HIGH)
    time.sleep(2)
    
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

auto_valgusfoor = [2, 3, 4]
ülejaanud = [21, 20, 16]
button = 26

GPIO.setup(auto_valgusfoor, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(ülejaanud, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(button, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
töö_on_tehtud = False


GPIO.add_event_detect(button, GPIO.RISING, callback)

while True:
    GPIO.output(ülejaanud[0], GPIO.HIGH)
    if button_pressed:
        GPIO.output(auto_valgusfoor[0], GPIO.HIGH)
        GPIO.output(ülejaanud[0], GPIO.LOW)
        GPIO.output(ülejaanud[1], GPIO.HIGH)
        töö_on_tehtud = True
    else:
        GPIO.output(auto_valgusfoor[0], GPIO.HIGH)
    time.sleep(5)
    if button_pressed:
        if töö_on_tehtud:
            button_pressed = False
            töö_on_tehtud = False
            GPIO.output(ülejaanud[2], GPIO.LOW)
            GPIO.output(ülejaanud[1], GPIO.LOW)
            GPIO.output(ülejaanud[0], GPIO.HIGH)

    yellow_car_ar()
    time.sleep(1)
    green_car()
    time.sleep(5)
    yellow_car_ag()
    GPIO.output(ülejaanud[0], GPIO.LOW)

GPIO.cleanup()
