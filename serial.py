import serial

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyUSB0', 9600, timeout = 1)
    ser.flush()
    
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            print(line)
            print(line[-4:-1])
            if int(line[-4:-1]) < 400:
                ser.write(b"Oh my! So close!\n")