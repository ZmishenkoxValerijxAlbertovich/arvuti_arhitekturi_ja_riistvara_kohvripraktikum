int trigger_pin = 2;
int echo_pin = 3;
int time;
int distance;

void setup ( ) {
  Serial.begin (9600);
  pinMode (trigger_pin, OUTPUT);
  pinMode (echo_pin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop ( ) {
  digitalWrite (trigger_pin, HIGH);
  delayMicroseconds (10);
  digitalWrite (trigger_pin, LOW);
  time = pulseIn (echo_pin, HIGH);
  distance = (time * 0.34) / 2; //distance in millimeters
  if (distance < 350) {
    delay(3300);
    if (distance < 350) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(1000);
      Serial.print ("Distance= ");
      Serial.println (distance);
    }
  } else {
    Serial.print ("Distance= ");
    Serial.println (distance);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  }
  if (Serial.available() > 0) {
    String data = Serial.readStringUntil('\n');
    Serial.println(data);
  }
}
