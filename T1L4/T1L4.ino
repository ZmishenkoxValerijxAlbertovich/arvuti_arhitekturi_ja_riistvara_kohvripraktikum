#include <Servo.h>
Servo frontBrakes;
Servo rearBrakes;
// Air pump connections
int in2 = 6;
// Valve connections
int in4 = 7;
int brakesOffAngle = 160;
int brakesOnAngle = 70;

void setup() {
  frontBrakes.attach(3);
  rearBrakes.attach(5);

  pinMode(in4, OUTPUT);
  digitalWrite(in4, HIGH);
  delay(500);
  pinMode(in2, OUTPUT);


}

void loop() {
  digitalWrite(in4, LOW);//extend
  frontBrakes.write(brakesOffAngle);
  rearBrakes.write(brakesOnAngle);
  delay(500);
  digitalWrite(in2, HIGH); 
  delay(5000);
  digitalWrite(in2, LOW);
  delay(500);
  digitalWrite(in4, HIGH);
  frontBrakes.write(brakesOnAngle);
  rearBrakes.write(brakesOffAngle);
  delay(1000);
  
  digitalWrite(in4, HIGH);
  delay(500);
  digitalWrite(in2, HIGH);
  delay(5000);
  digitalWrite(in2, LOW);
  delay(500);
  digitalWrite(in4, HIGH);
  delay(500); 

}
