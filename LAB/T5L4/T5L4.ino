#include <Wire.h>
#include <Servo.h>
#include <LSM6.h>

// Create instances for each sensor.
LSM6 imu;
Servo contServo;


void setup() {
  // Put your setup code here, to run once:
  // Set up communication with computer...
  Serial.begin(9600);
  contServo.attach(5);
  // and with the sensor:
  Wire.begin();

  // Initialize the IMU (accelerometer and gyroscope).
  if (!imu.init())
  {
    // If we fail to detect the sensor we print this information to user and don't continue with the program.
    Serial.println("Failed to detect and initialize IMU!");
    while (true);
  }

  // Enable all the sensors and set default configurations.

  // Turns on the accelerometer and gyro and enables a consistent set of default settings.
  // This function will reset the accelerometer to ±2 g full scale and the gyro to ±245 dps.
  imu.enableDefault();

}

void loop() {
  // Put your main code here, to run repeatedly:
  // continuously poll the sensors, convert the results to human-understandable units and send the results over serial.

  // IMU:

  imu.read();

  float accel = imu.a.x * 0.061 / 1000;

  if (accel < 1400) {
    contServo.writeMicroseconds(1400);
  } else if (accel > 1600) {
    contServo.writeMicroseconds(1600);
  }
  else{
    int speedServ = 1500 + accel * 100;
    contServo.writeMicroseconds(speedServ);
  }

  // Some delay to slow down the output
  delay(1000);
}
