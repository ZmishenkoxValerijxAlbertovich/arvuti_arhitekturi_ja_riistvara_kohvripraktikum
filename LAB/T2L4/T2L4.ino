#include <Servo.h>
Servo contServo;
int userInput;

void setup() {
  Serial.begin(9600);

  contServo.attach(5);

}

void loop() {
  if (Serial.available() > 0) {
    userInput = Serial.parseInt();
    if(userInput >= 1400 && userInput <= 1600){
      contServo.writeMicroseconds(userInput);
      }
  }
}
