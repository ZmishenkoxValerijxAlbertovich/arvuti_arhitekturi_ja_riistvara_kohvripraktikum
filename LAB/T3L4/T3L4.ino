#include <Servo.h>

Servo contServo;
unsigned long currentMillis;
unsigned long previousMillis = 0;
unsigned long tiemOfRevolution;
unsigned long shappeared;
bool firstShade = false;
bool secondShade = false;
int inShade = 80;
int outOfShade = 220;
int input;

void showAtenus(int reading) {
  if (currentMillis - previousMillis >= 10) {
    previousMillis = currentMillis;
    Serial.println(reading);
  }
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  contServo.attach(5);
}

// the loop routine runs over and over again forever:
void loop() {

  currentMillis = millis();
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  showAtenus(sensorValue);
  if (Serial.available() > 0) {
    input = Serial.parseInt();
    if (input >= 1400 && input <= 1600) {
      contServo.writeMicroseconds(input);
    }
  }
  if (sensorValue < 80 && secondShade && firstShade ) {
    unsigned long timeOfRevolution = shappeared - currentMillis;
    float revolutionPerMinute = 1 / int(timeOfRevolution) * 1000 * 60;
    Serial.print("RPM: ");
    Serial.println(revolutionPerMinute);
    firstShade = false;
    secondShade = false;

  } else if (sensorValue < 80) {
    shappeared = currentMillis;
    firstShade = true;
  }
  else if (sensorValue > 220) {
    secondShade = true;
  }


}
