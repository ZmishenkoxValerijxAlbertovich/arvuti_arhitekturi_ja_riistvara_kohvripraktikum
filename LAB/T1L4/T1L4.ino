#include <Servo.h>

Servo frontBrakes;
Servo rearBrakes;
// Air pump connections
int pump = 6;
// Valve connections
int valve = 7;
int brakesOffAngle = 160;
int brakesOnAngle = 70;

void setup() {
  frontBrakes.attach(3);
  rearBrakes.attach(5);

  pinMode(valve, OUTPUT);
  digitalWrite(valve, HIGH);
  delay(500);
  pinMode(pump, OUTPUT);


}

void loop() {
  digitalWrite(valve, LOW);//extend
  frontBrakes.write(brakesOffAngle);
  rearBrakes.write(brakesOnAngle);
  delay(500);
  digitalWrite(pump, HIGH);
  delay(5000);
  digitalWrite(pump, LOW);
  delay(500);
  digitalWrite(valve, HIGH);
  frontBrakes.write(brakesOnAngle);
  rearBrakes.write(brakesOffAngle);
  delay(1000);

  digitalWrite(valve, HIGH);
  delay(500);
  digitalWrite(pump, HIGH);
  delay(5000);
  digitalWrite(pump, LOW);
  delay(500);
  digitalWrite(valve, HIGH);
  delay(500);

}
