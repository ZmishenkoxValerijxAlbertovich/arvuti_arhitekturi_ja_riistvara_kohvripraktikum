
int ledPin = 9;  // LED connected to digital pin 9

void setup() {
  Serial.begin(9600);
}

void loop() {

  // read the input 10 times on analog pin 5 and get the average:
  int sensorReadings = 0;
  for (int i = 0; i < 10; i++) {
    sensorReadings += analogRead(A5);
  }
  int averageReading = sensorReadings / 10;
  // remap the analog values to a range corresponding to 8 bit.
  // And inverse the range so that high reading (high light intensity) corresponds to low value (dull LED light or LED is off)
  int fadeValue = map(averageReading, 0, 1023, 255, 0);
  if (fadeValue < 10) {
    analogWrite(ledPin, 0);
  } else {
    analogWrite(ledPin, fadeValue);
  }

}
