import RPi.GPIO as GPIO
import time

button_pressed = False # Globaalne muutuja, mis garanteerib seda, et
# nuppule vastav funktsioon töötas ja toimis üks korda


def yellow_car_ar():  # Kollane tuli sütib pärast punast tuld
    GPIO.output(traffic_light[0], GPIO.LOW)
    check_button()
    GPIO.output(traffic_light[1], GPIO.HIGH)


def green_car():
    GPIO.output(traffic_light[1], GPIO.LOW)
    check_button()
    GPIO.output(traffic_light[2], GPIO.HIGH)


def yellow_car_ag():  # Kollane tuli sütib pärast rohelist tuld
    GPIO.output(traffic_light[2], GPIO.LOW)
    for i in range(2):
        check_button()
        GPIO.output(traffic_light[1], GPIO.HIGH)
        check_button()
        time.sleep(0.4)
        check_button()
        GPIO.output(traffic_light[1], GPIO.LOW)
        check_button()
        time.sleep(0.4)
    check_button()
    GPIO.output(traffic_light[1], GPIO.HIGH)
    check_button()
    time.sleep(0.4)
    check_button()
    GPIO.output(traffic_light[1], GPIO.LOW)


def check_button():
    global button_pressed# Viide globaalsele muutujale, mis teeb kindlaks,
    # et globaalse mutuuja väärtus oleks ümberkirjutatud
    if GPIO.input(26):
        button_pressed = True
        GPIO.output(blue_light, GPIO.HIGH)


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

traffic_light = [2, 3, 4]  # indeksi järgi: 0 - punane, 1 - kollane, 2 - roheline
pedestrian_light = [21, 20]  # indeksi järgi: - punane, 20 - roheline
blue_light = 16
button = 26

GPIO.setup(traffic_light, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(pedestrian_light, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(blue_light, GPIO.OUT)
GPIO.setup(button, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

while True:
    check_button()
    GPIO.output(pedestrian_light[0], GPIO.HIGH)
    check_button()
    # Siin toimub kontroll, kas jalakäijate punane peab ainult sütima või see muutub roheliseks
    # Vastav olukord on pandud tööle
    if button_pressed:
        check_button()
        GPIO.output(blue_light, GPIO.LOW)
        check_button()
        GPIO.output(traffic_light[0], GPIO.HIGH)
        check_button()
        GPIO.output(pedestrian_light[0], GPIO.LOW)
        check_button()
        GPIO.output(pedestrian_light[1], GPIO.HIGH)
        check_button()
        time.sleep(5)
        check_button()
        GPIO.output(pedestrian_light[1], GPIO.LOW)
        check_button()
        GPIO.output(pedestrian_light[0], GPIO.HIGH)
        check_button()
        button_pressed = False  # Muutuja väärtus on seatud False'ks, et järgmine tsükli iteraatsioon oli tavapärane
    else:
        check_button()
        GPIO.output(traffic_light[0], GPIO.HIGH)
        check_button()
        time.sleep(5)
    check_button()    
    yellow_car_ar()
    check_button()
    time.sleep(1)
    check_button()
    green_car()
    check_button()
    time.sleep(5)
    check_button()
    yellow_car_ag()
    check_button()
   