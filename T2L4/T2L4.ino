#include <Servo.h>
Servo contServo;
int input;

void setup() {
  Serial.begin(9600);

  contServo.attach(4);

}

void loop() {
  if (Serial.available() > 0) {
    input = Serial.parseInt();
    if(input >= 1400 && input <= 1600){
      contServo.writeMicroseconds(input);
      }
  }
}
