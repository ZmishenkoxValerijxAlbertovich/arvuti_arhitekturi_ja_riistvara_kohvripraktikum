#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Check with marker
import numpy as np
import cv2
from time import time


lB = 0
lG = 0
lR = 80
hB = 50        
hG = 120
hR = 230

thresholded_window = 'RGB detection limits'
cv2.namedWindow(thresholded_window, cv2.WINDOW_GUI_NORMAL)

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.minDistBetweenBlobs = 200
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByColor = True
blobparams.blobColor = 255
detector = cv2.SimpleBlobDetector_create(blobparams)

fps_msg = "Thinking..."

# Define callback function
def update_value(val):
    global lR, lG, lB, hR, hG, hB
    # get current trackbar position
    lR = cv2.getTrackbarPos("Low detection limit for Red", thresholded_window)
    lG = cv2.getTrackbarPos("Low detection limit for Green", thresholded_window)
    lB = cv2.getTrackbarPos("Low detection limit for Blue", thresholded_window)
    hR = cv2.getTrackbarPos("Upper detection limit for Red", thresholded_window)
    hG = cv2.getTrackbarPos("Upper detection limit for Green", thresholded_window)
    hB = cv2.getTrackbarPos("Upper detection limit for Blue", thresholded_window)
    
def main():
    # Open the camera
    cap = cv2.VideoCapture(0)
    # Create windows
    cv2.namedWindow("Original")
    
    global lR, lG, lB, hR, hG, hB
    cv2.createTrackbar("Low detection limit for Red", thresholded_window, lR, 255, update_value)
    cv2.createTrackbar("Low detection limit for Green", thresholded_window, lG, 255, update_value)
    cv2.createTrackbar("Low detection limit for Blue", thresholded_window, lB, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Red", thresholded_window, hR, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Green", thresholded_window, hG, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Blue", thresholded_window, hB, 255, update_value)
    
    number_of_frames = 0
    fps_msg = "Thinking..."
    initial_time_instance = time()
    while True:
        # Read the image from the camera
        ret, frame = cap.read()

        # Colour detection limits
        lowerLimits = np.array([lB, lG, lR])
        upperLimits = np.array([hB, hG, hR])

        # Our operations on the frame come here
        thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
        
        new_time_instance = time()
        if new_time_instance - initial_time_instance > 1:
            fps = int(number_of_frames / (new_time_instance - initial_time_instance))
            initial_time_instance = new_time_instance
            number_of_frames = 0
            fps_msg = "FPS: " + str(fps)
        cv2.putText(thresholded, fps_msg, (50, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
        
        keypoints = detector.detect(thresholded)
        for keypoint in keypoints:
            x = keypoint.pt[0]
            y = keypoint.pt[1]
            coords = "("+str(int(x))+","+str(int(y))+")"
            cv2.putText(frame, coords, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 100, 255), 1)

        frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (255,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        
        number_of_frames += 1
        
        cv2.imshow('Original', frame)
    
        # Display the resulting frame
        cv2.imshow(thresholded_window, thresholded)
        
        # Quit the program when 'q' is pressed
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            break

    # When everything done, release the capture
    print('Closing program')
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
