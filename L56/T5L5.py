#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Check with marker
import numpy as np
import cv2
from time import time


lH = 0
lS = 0
lV = 0
hH = 255
hS = 255
hV = 255

try:
    with open("trackbar_defaults.txt") as file:
        lH = int(file.readline())
        lS = int(file.readline())
        lV = int(file.readline())
        hH = int(file.readline())
        hS = int(file.readline())
        hV = int(file.readline())
except FileNotFoundError:
    pass


thresholded_window = 'HSV detection limits'
cv2.namedWindow(thresholded_window, cv2.WINDOW_GUI_NORMAL)

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.minDistBetweenBlobs = 200
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
blobparams.filterByColor = True
blobparams.blobColor = 255
detector = cv2.SimpleBlobDetector_create(blobparams)

fps_msg = "Thinking..."

# Define callback function
def update_value(val):
    global lH,lS,lV,hH,hS,hV
    # get current trackbar position
    lH = cv2.getTrackbarPos("Low detection limit for Hue", thresholded_window)
    lS = cv2.getTrackbarPos("Low detection limit for Saturation", thresholded_window)
    lV = cv2.getTrackbarPos("Low detection limit for Value", thresholded_window)
    hH = cv2.getTrackbarPos("Upper detection limit for Hue", thresholded_window)
    hS = cv2.getTrackbarPos("Upper detection limit for Saturation", thresholded_window)
    hV = cv2.getTrackbarPos("Upper detection limit for Value", thresholded_window)

def threshold_val_to_file(lH,lS,lV,hH,hS,hV):
    with open("trackbar_defaults.txt", 'w', encoding = "UTF-8") as file:
        file.write(str(lH) +'\n')
        file.write(str(lS) +'\n')
        file.write(str(lV) +'\n')
        file.write(str(hH) +'\n')
        file.write(str(hS) +'\n')
        file.write(str(hV))
    
def main():
    # Open the camera
    cap = cv2.VideoCapture(0)
    # Create windows
    cv2.namedWindow("Original")
    
    global lH, lS, lV, hH, hS, hV
    cv2.createTrackbar("Low detection limit for Hue", thresholded_window, lH, 255, update_value)
    cv2.createTrackbar("Low detection limit for Saturation", thresholded_window, lS, 255, update_value)
    cv2.createTrackbar("Low detection limit for Value", thresholded_window, lV, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Hue", thresholded_window, hH, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Saturation", thresholded_window, hS, 255, update_value)
    cv2.createTrackbar("Upper detection limit for Value", thresholded_window, hV, 255, update_value)
    
    number_of_frames = 0
    fps_msg = "Thinking..."
    initial_time_instance = time()
    while True:
        # Read the image from the camera
        ret, frame = cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Colour detection limits
        lowerLimits = np.array([lH, lS, lV])
        upperLimits = np.array([hH, hS, hV])
        
        # Our operations on the frame come here
        thresholded = cv2.inRange(frame, lowerLimits, upperLimits)

        new_time_instance = time()
        if new_time_instance - initial_time_instance > 1:
            fps = int(number_of_frames / (new_time_instance - initial_time_instance))
            initial_time_instance = new_time_instance
            number_of_frames = 0
            fps_msg = "FPS: " + str(fps)
        cv2.putText(thresholded, fps_msg, (50, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
        
        keypoints = detector.detect(thresholded)
        for keypoint in keypoints:
            x = keypoint.pt[0]
            y = keypoint.pt[1]
            coords = "("+str(int(x))+","+str(int(y))+")"
            cv2.putText(frame, coords, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 100, 255), 1)
        
        frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (255,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        
        number_of_frames += 1
        
        cv2.imshow('Original', frame)
    
        # Display the resulting frame
        cv2.imshow(thresholded_window, thresholded)
        
        # Quit the program when 'q' is pressed
        if (cv2.waitKey(1) & 0xFF) == ord('q'):
            threshold_val_to_file(lH,lS,lV,hH,hS,hV)
            break

    # When everything done, release the capture
    print('Closing program')
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()