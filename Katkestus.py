import RPi.GPIO as GPIO
import time

button_pressed = False # Globaalne muutuja, mis garanteerib seda, et
# nuppule vastav funktsioon töötas ja toimis üks korda


def yellow_car_ar():  # Kollane tuli sütib pärast punast tuld
    GPIO.output(traffic_light[0], GPIO.LOW)
    GPIO.output(traffic_light[1], GPIO.HIGH)


def green_car():
    GPIO.output(traffic_light[1], GPIO.LOW)
    GPIO.output(traffic_light[2], GPIO.HIGH)


def yellow_car_ag():  # Kollane tuli sütib pärast rohelist tuld
    GPIO.output(traffic_light[2], GPIO.LOW)
    for i in range(2):
        GPIO.output(traffic_light[1], GPIO.HIGH)
        time.sleep(0.4)
        GPIO.output(traffic_light[1], GPIO.LOW)
        time.sleep(0.4)
    GPIO.output(traffic_light[1], GPIO.HIGH)
    time.sleep(0.4)
    GPIO.output(traffic_light[1], GPIO.LOW)


def callback(chanel):
    global button_pressed # Viide globaalsele muutujale, mis teeb kindlaks,
    # et globaalse mutuuja väärtus oleks ümberkirjutatud
    button_pressed = True
    GPIO.output(blue_light, GPIO.HIGH)


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

traffic_light = [2, 3, 4]  # indeksi järgi: 0 - punane, 1 - kollane, 2 - roheline
pedestrian_light = [21, 20]  # indeksi järgi: - punane, 20 - roheline
blue_light = 16
button = 26

GPIO.setup(traffic_light, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(pedestrian_light, GPIO.OUT, initial = GPIO.LOW)
GPIO.setup(blue_light, GPIO.OUT)
GPIO.setup(button, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

GPIO.add_event_detect(button, GPIO.RISING, callback)

while True:
    GPIO.output(pedestrian_light[0], GPIO.HIGH)

    # Siin toimub kontroll, kas jalakäijate punane peab ainult sütima või see muutub roheliseks
    # Vastav olukord on pandud tööle
    if button_pressed:
        GPIO.output(blue_light, GPIO.LOW)
        GPIO.output(traffic_light[0], GPIO.HIGH)
        GPIO.output(pedestrian_light[0], GPIO.LOW)
        GPIO.output(pedestrian_light[1], GPIO.HIGH)
        time.sleep(5)
        GPIO.output(pedestrian_light[1], GPIO.LOW)
        GPIO.output(pedestrian_light[0], GPIO.HIGH)
        button_pressed = False  # Muutuja väärtus on seatud False'ks, et järgmine tsükli iteraatsioon oli tavapärane
    else:
        GPIO.output(traffic_light[0], GPIO.HIGH)
        time.sleep(5)

    yellow_car_ar()
    time.sleep(1)
    green_car()
    time.sleep(5)
    yellow_car_ag()